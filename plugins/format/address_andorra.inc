<?php

/**
 * @file
 * A specific handler for AD.
 */

$plugin = array(
  'title' => t('Address form (AD add-on)'),
  'format callback' => 'addressfield_format_address_andorra_generate',
  'type' => 'address',
  'weight' => -80,
);

function addressfield_format_address_andorra_generate(&$format, $address, $context = array()) {
  if ($address['country'] == 'AD') {
    if (isset($format['locality_block']['administrative_area'])) {
      $state_options = array(
        ''   => t('--'),
        'C'  => t('Andorra la Vella'),
        'VI' => t('Canillo'),
        'AB' => t('Encamp'),
        'A'  => t('Escaldes-Engordany'),
        'AL' => t('La Massana'),
        'O'  => t('Ordino'),
        'AV' => t('Sant Julià de Lòria'),
      );

      if (!empty($format['locality_block']['administrative_area']['#options'])) {
        $state_options += $format['locality_block']['administrative_area']['#options'];
      }

      $format['locality_block']['administrative_area']['#title'] = t('Province');
      $format['locality_block']['administrative_area']['#options'] = $state_options;
    }
  }
}
